let
  pkgs = import <nixpkgs> {};

  jobs = rec {

    tarball =
      pkgs.releaseTools.sourceTarball {
        name = "myrandom-tarball";
        src = ./.;
        buildInputs = [ pkgs.cabal-install ];
        distPhase = ''
          cabal sdist
          mkdir -p $out/tarballs/
          cp dist/*.tar.gz $out/tarballs/
        '';
      };

    build =
      { system ? builtins.currentSystem }:
      let 
        pkgs = import <nixpkgs> { inherit system; }; 
      in
      pkgs.haskellPackages.callCabal2nix "myrandom" ./. {};

  };

in
  jobs

