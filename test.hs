import Test.Hspec

main :: IO ()
main = hspec $ do

  describe "suite1" $ do
    it "test1" $ 21*2 `shouldBe` 42
    it "test2" $ sqrt 1764 `shouldBe` 42

  describe "suite2" $ do
    it "test1" $ reverse "toto" `shouldBe` "otot"
    it "test2" $ reverse "radar" `shouldBe` "radar"

